%% Author: Luiz Ricardo Takeshi Horita
% Date: August 10-th, 2017
% =========================================================================

addpath('./functions/');
import drawArrow.*
import drawAxes.*
import drawCube.*
import drawCamera.*

%% Initialize parameters
% Cube parameters:
cubePose = [10, 15, 13, 1, 2, 3];
cubeSize = 5;
cubeAlpha = 0.9;

% Camera parameters:
camPose = [35 ,35 ,30 , 1.0, -2.5, -0.5];
focalDist = 8.5;
pxlSize = 0.01;
resolution = [480 640];
sizeRatio = 800;
camAlpha = 0.8;

%% Draw everything
Draw the world's coordinate system
drawAxes([0,0,0,0,0,0], [0.5, 0.5, 0.5], 0.008, ['c','y','m']);

% Draw the camera model
drawCamera(camPose, focalDist, pxlSize, resolution, sizeRatio, camAlpha);

% Draw the cube
corners = drawCube(cubePose, cubeSize, 'g',cubeAlpha);

% Setup graph
xlim([-1 1]);
ylim([-1 1]);
zlim([-1 2]);
% box on;
xlabel('x'); ylabel('y'); zlabel('z');
