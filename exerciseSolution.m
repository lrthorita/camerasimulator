%% Author: Luiz Ricardo Takeshi Horita
% Date: August 10-th, 2017
% =========================================================================

addpath('./functions/');
import drawArrow.*
import drawAxes.*
import drawCube.*
import drawCamera.*

%% Initialize parameters
% Cube parameters:
cubePose = [1.25, 1.25, 0.25, 0, 0, 0];
cubeSize = 0.5;
cubeAlpha = 0.9;
% cubePose = [10, 15, 13, 1, 2, 3];
% cubeSize = 5;
% cubeAlpha = 0.9;

% Camera parameters:
camPose = [-1, 1, 5, 0, 160*pi/180, 0];
focalDist = 16;
pxlSize = 0.01;
resolution = [480 640];
sizeRatio = 100;
camAlpha = 0.8;
% camPose = [35 ,35 ,30 , 1.0, -2.5, -0.5];
% focalDist = 8.5;
% pxlSize = 0.01;
% resolution = [480 640];
% sizeRatio = 800;
% camAlpha = 0.8;

%% Draw everything
% Draw the world's coordinate system
drawAxes([0,0,0,0,0,0], [0.5, 0.5, 0.5], 0.008, ['c','y','m']);

% Draw the camera model
drawCamera(camPose, focalDist, pxlSize, resolution, sizeRatio, camAlpha);

% Draw the cube
corners = drawCube(cubePose, cubeSize, 'g',cubeAlpha);

% Setup graph
xlim([-1.5 2.5]);
ylim([-1.5 2.5]);
zlim([-0.5 5.5]);
box on;
xlabel('x'); ylabel('y'); zlabel('z');