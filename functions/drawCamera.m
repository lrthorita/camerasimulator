%% Author: Luiz Ricardo Takeshi Horita
% Date: August 10-th, 2017

function drawCamera(pose, focalDist, pxlSize, resolution, sizeRatio, alpha)
%% ========================================================================
%   drawCamera(pose, focalDist, resolution, sizeRatio, alpha) -> nothing
% -------------------------------------------------------------------------
% DESCRIPTION:
%   This function draw/plot the projective camera representation in 3D
%   space.
% 
% INPUT:
%   - pose       = A vector [x, y, z,t_x, t_y, t_z] indicating the position
%                  of the center of projection and the orientation of the
%                  camera. The position values must be in meters, and the 
%                  rotation angles in radians;
%   - focalDist  = The focal distance (in milimeters) of the camera;
%   - pxlSize    = The size (in milimeters) of each pixel;
%   - resolution = The camera resolution [height, width] in pixels.
%   - sizeRatio  = The size ratio of the camera model. If you want a bigger
%                  representation of the camera model, use sizeRatio > 1.
%                  1, it will show the camera model in "real" size.
%   - alpha      = Transparency of the drawing.
%% ========================================================================

focalDist = focalDist/1000;
pxlSize = pxlSize/1000;

% Compute the image plane size:
height = pxlSize*resolution(1);
width = pxlSize*resolution(2);

%% Draw axes of the camera coordinate system
axes_lengths = [0.8*width*sizeRatio, 0.8*height*sizeRatio, 1.2*focalDist*sizeRatio];
drawAxes(pose,axes_lengths, 0.00006*sizeRatio, ['r','g','b']);

%% Pyramid for image plane illustration
% Initialize a pyramid with its peak at the [0.5, 0.5, 0.0] position:
%   *P.S.: Each colum correspond to the coordinates of a face corners.
X_pyr = [0.5 0.5 0.5 0.5; 0 1 1 0; 1 1 0 0];
Y_pyr = [0.5 0.5 0.5 0.5; 0 0 1 1; 0 1 1 0];
Z_pyr = [0.0 0.0 0.0 0.0; 1 1 1 1; 1 1 1 1];

X_base = [0; 1; 1; 0];
Y_base = [0; 0; 1; 1];
Z_base = [1; 1; 1; 1];

% Resize the pyramid and move it to the set position:
X_pyr = sizeRatio * width * (X_pyr - 0.5);
Y_pyr = sizeRatio * height * (Y_pyr - 0.5);
Z_pyr = sizeRatio * focalDist * Z_pyr;

X_base = sizeRatio * width * (X_base - 0.5);
Y_base = sizeRatio * height * (Y_base - 0.5);
Z_base = sizeRatio * focalDist * Z_base;

% Concatenate every corners coordinates to a 3x(12+4) matrix.
corners_pyr = [reshape(X_pyr,1,12); reshape(Y_pyr,1,12); reshape(Z_pyr,1,12)];
corners_base = [reshape(X_base,1,4); reshape(Y_base,1,4); reshape(Z_base,1,4)];
corners = [corners_pyr corners_base];

% Get rotation matrix
t_x = pose(4); t_y = pose(5); t_z = pose(6); % Rotation angles

Rx = [1 0 0; 0 cos(t_x) -sin(t_x); 0 sin(t_x) cos(t_x)];
Ry = [cos(t_y) 0 sin(t_y); 0 1 0; -sin(t_y) 0 cos(t_y)];
Rz = [cos(t_z) -sin(t_z) 0; sin(t_z) cos(t_z) 0; 0 0 1];
R = Rz * Ry * Rx; % Rotation matrix

% Rotate and translate pyramid
corners = R * corners;
corners(1,:) = corners(1,:) + pose(1);
corners(2,:) = corners(2,:) + pose(2);
corners(3,:) = corners(3,:) + pose(3);

% Re-organize the coordinates in matrices to plot with "fill3" function
X_pyr_new =reshape(corners(1,1:12),3,4);
Y_pyr_new =reshape(corners(2,1:12),3,4);
Z_pyr_new =reshape(corners(3,1:12),3,4);

X_base_new =reshape(corners(1,13:16),4,1);
Y_base_new =reshape(corners(2,13:16),4,1);
Z_base_new =reshape(corners(3,13:16),4,1);

% Plot the pyramid in 3D space
C_pyr = [0.5 0.5 1.0];
C_base = [0.5 1.0 0.5];
fill3(X_pyr_new, Y_pyr_new, Z_pyr_new, C_pyr, 'FaceAlpha', alpha/2); hold on;
fill3(X_base_new, Y_base_new, Z_base_new, C_base, 'FaceAlpha', alpha);







