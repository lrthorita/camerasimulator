%% Author: Luiz Ricardo Takeshi Horita
% Date: August 10-th, 2017
% =========================================================================

function drawAxes(pose, lengths, width, colors)
%% ========================================================================
%                 drawAxes(pose, length, width, colors)
% -------------------------------------------------------------------------
% DESCRIPTION:
%   This function draw/plot the x, y and z axes of a coordinate system for
%   a given pose in the world coordinate system.
% 
% INPUT:
%   - pose       = An array [x, y, z,t_x, t_y, t_z] indicating the position
%                  of the origin and the orientation of the coordinate
%                  system. The position values are given in meters, and the 
%                  rotation angles in radians;
%   - lengths    = An array [l_x l_y l_z] setting the axes lengths;
%   - width      = The axes width;
%   - colors     = A vector of strings with 3 colors for each axe, 
%                  [x_color y_color z_color]. The
%                  colors must be according to MATLAB specification (see 
%                  MATLAB help item 'ColorSpec')
% =========================================================================
T = pose(1:3)'; % The coordinate system position

t_x = pose(4); t_y = pose(5); t_z = pose(6); % Angles of rotations

% Rotation matrix
Rx = [1 0 0; 0 cos(t_x) -sin(t_x); 0 sin(t_x) cos(t_x)];
Ry = [cos(t_y) 0 sin(t_y); 0 1 0; -sin(t_y) 0 cos(t_y)];
Rz = [cos(t_z) -sin(t_z) 0; sin(t_z) cos(t_z) 0; 0 0 1];
R = Rz * Ry * Rx;

% Initialize the ending points of the axes
origin_axes = [lengths(1) 0 0; 0 lengths(2) 0; 0 0 lengths(3)];

% Rotate and translate the axes ending points
set_axes = R * origin_axes + [T, T, T];

x_axe = drawArrow(T', set_axes(:,1)', 'stemWidth', width, 'color', colors(1)); hold on;
y_axe = drawArrow(T', set_axes(:,2), 'stemWidth', width, 'color', colors(2)); hold on;
z_axe = drawArrow(T', set_axes(:,3), 'stemWidth', width, 'color', colors(3));

%% Sphere for the origin
[x_c,y_c,z_c] = sphere;
r = 0.05*min(lengths);
surf(x_c*r + T(1), y_c*r + T(2), z_c*r + T(3), 'FaceColor','red'); hold on;