% Author: Luiz Ricardo Takeshi Horita
% Date: August 10-th, 2017

function corners = drawCube(pose, size, color, alpha)
%% ========================================================================
%         drawCube(pose, size, color, alpha) -> corners
% -------------------------------------------------------------------------
% DESCRIPTION:
%   This function draw/plot the set cube in 3D space and return the
%   coordinates of every corners concatenated in a matrix.
%   *P.S.: Metric values (coordinates and size) are in meters.
% 
% INPUT:
%   - pose  = A vector [x, y, z,t_x, t_y, t_z] indicating the center
%             position of the cube and the orientation of the cube. The
%             rotation angles must be given in radians;
%   - size  = The cube size (length of each edges);
%   - color = 'r' for red, 'g' for green, and 'b' for blue;
%   - alpha = Transparency of faces.
% 
% OUTPUT:
%   - corners = A 8x3 matrix, where each row correspond to the coordinate
%               of one corner of the cube.
%% ========================================================================
t_x = pose(4); t_y = pose(5); t_z = pose(6);

% Rotation matrix
Rx = [1 0 0; 0 cos(t_x) -sin(t_x); 0 sin(t_x) cos(t_x)];
Ry = [cos(t_y) 0 sin(t_y); 0 1 0; -sin(t_y) 0 cos(t_y)];
Rz = [cos(t_z) -sin(t_z) 0; sin(t_z) cos(t_z) 0; 0 0 1];
R = Rz * Ry * Rx;

% Translation vector
% T = [pose(1); pose(2); pose(3)];

% Initialize unitary cube centralized at [0.5, 0.5, 0.5] position:
%   *P.S.: Each colum correspond to the coordinates of a face corners.
X = [0 0 0 0 0 1; 1 0 1 1 1 1; 1 0 1 1 1 1; 0 0 0 0 0 1];
Y = [0 0 0 0 1 0; 0 1 0 0 1 1; 0 1 1 1 1 1; 0 0 1 1 1 0];
Z = [0 0 1 0 0 0; 0 0 1 0 0 0; 1 1 1 0 1 1; 1 1 1 0 1 1];

% Resize the cube and move it to the set position:
X = size*(X-0.5);
Y = size*(Y-0.5);
Z = size*(Z-0.5);

% Concatenate every corners coordinates to a 3x24 matrix.
corners = [reshape(X,1,24); reshape(Y,1,24); reshape(Z,1,24)];

% Rotate and translate the cube
corners = R * corners;
corners(1,:) = corners(1,:) + pose(1);
corners(2,:) = corners(2,:) + pose(2);
corners(3,:) = corners(3,:) + pose(3);

% Re-organize the coordinates in matrices to plot with "fill3" function
X_new =reshape(corners(1,:),4,6);
Y_new =reshape(corners(2,:),4,6);
Z_new =reshape(corners(3,:),4,6);

% Set the cube color
if color == 'r'
    C = [1.0 0.0 0.0];
elseif color == 'g'
    C = [0.0 1.0 0.0];
elseif color == 'b'
    C = [0.0 0.0 1.0];
end

% Plot the cube in 3D space
fill3(X_new, Y_new, Z_new, C, 'FaceAlpha', alpha);



