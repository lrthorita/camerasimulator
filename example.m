%% Author: Luiz Ricardo Takeshi Horita
% Date: August 10-th, 2017
% =========================================================================
% Load functions
addpath('./functions/');
import drawArrow.*
import drawAxes.*
import drawCube.*
import drawCamera.*

%% Initialize parameters
% Cube parameters:
cubePose = [0, 0, 0, 0, 0, 0];  % Coordinates in meters. Ângles in radians.
cubeSize = 0.5;                 % in meters
cubeAlpha = 0.8;                % Transparency level

% Camera parameters:
camPose = [-0.25, 0.25, 5, pi, 0, 0]; % Coordinates in meters. Ângles in radians.
focalDist = 16;                 % in milimeters
pxlSize = 0.01;                 % in milimeters
resolution = [480 640];         % in pixels
sizeRatio = 100;                % Represent camera 100 times bigger
camAlpha = 0.8;                 % Transparency level

%% Draw everything
% Draw the world's coordinate system to use as reference
%   - Colors: x in cyan, y in yellow, z in magenta.
drawAxes([0,0,0,0,0,0], [0.5, 0.5, 0.5], 0.008, ['c','y','m']);

% Draw the camera model
drawCamera(camPose, focalDist, pxlSize, resolution, sizeRatio, camAlpha);

% Draw the cube
corners = drawCube(cubePose, cubeSize, 'g',cubeAlpha);

% Setup graph
xlim([-1.5 1.5]);
ylim([-1.5 1.5]);
zlim([-0.5 5.5]);
box on;
xlabel('x'); ylabel('y'); zlabel('z');